require 'spec_helper'

describe 'weblate::webserver' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_service('apache2').with(
          ensure: 'running',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2dismod rewrite').with(
          onlyif: '/usr/bin/test -f /etc/apache2/mods-enabled/rewrite.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod proxy').with(
          creates: '/etc/apache2/mods-enabled/proxy.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod headers').with(
          creates: '/etc/apache2/mods-enabled/headers.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod proxy_http').with(
          creates: '/etc/apache2/mods-enabled/proxy_http.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/var/www/weblate').with(
          ensure: 'directory',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/www/weblate/robots.txt').with(
          ensure: 'file',
          owner: 2000000,
          group: 2000000,
          mode: '0644',
          content: "User-agent: *\nDisallow: /translate\n",
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/apache-vhost.conf').with(
          ensure: 'file',
          content: "# -*- mode: apache; -*-\n# This is a config file for weblate managed by the weblate class.\n#\n# VirtualHost for weblate\n#\n\n<VirtualHost *:80>\n\n    ServerName translate.tails.boum.org\n    ServerAdmin tails-weblate@boum.org\n    # Add vhost name to log entries:\n    LogFormat \"%h %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-agent}i\\\"\" vhost_combined\n\n    CustomLog /var/log/apache2/access.log vhost_combined\n    LogLevel warn\n    ErrorLog /var/log/apache2/error.log\n\n    ErrorDocument 403 \"<h2>Forbidden</h2><p>Our slightly paranoid security system did not trust what you were doing there and blocked your request. If you feel this is incorrect, please contact tails-sysadmins [at] boum [dot] org. Be sure to mention the exact URL you are trying to reach (copy paste the address bar in your browser), as well as the time (including your timezone), so we can search our logs to see what went wrong. Our apologies for the inconvenience.</p>\"\n\n    ProxyPass /robots.txt !\n    Alias /robots.txt /var/www/weblate/robots.txt\n\n    # XXX: Always ensure Cookies have \"Secure\" set (JAH 2012/1)\n\n    # Reverse-proxy all requests to Weblate Docker container\n    ProxyPreserveHost On\n    ProxyPass \"/\"  \"http://127.0.0.1:8080/\"\n    ProxyPassReverse \"/\"  \"http://127.0.0.1:8080/\"\n\n</VirtualHost>\n\n<VirtualHost *:80>\n    ServerName staging.tails.boum.org\n    ServerAdmin tails-sysadmins@boum.org\n\n    DocumentRoot /var/www/staging\n\n    Header Set X-Robots-Tag \"none\"\n\n    <Directory  /var/www/staging/>\n        DirectoryIndex index.en.html\n        Order deny,allow\n        Allow from all\n    </Directory>\n    CustomLog /dev/null combined\n    ErrorLog /dev/null\n</VirtualHost>\n",
          owner: 'root',
          group: 2000000,
          mode: '0664',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/apache2/sites-available/000-default.conf').with(
          ensure: 'symlink',
          target: '/var/lib/weblate/config/apache-vhost.conf',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/modsecurity/modsecurity.conf').with(
          ensure: 'file',
          source: 'puppet:///modules/weblate/modsecurity/modsecurity.conf',
          owner: 'root',
          group: 'root',
          mode: '0644',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/modsecurity/crs/crs-setup.conf').with(
          ensure: 'file',
          source: 'puppet:///modules/weblate/modsecurity/crs-setup.conf',
          owner: 'root',
          group: 'root',
          mode: '0644',
          notify: 'Service[apache2]',
        )
      end
    end
  end
end
