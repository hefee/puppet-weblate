require 'spec_helper'

describe 'weblate::redis' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      redis_password: '456'
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_package('redis-server').with(
          ensure: 'installed',
        )
      end

      it do
        is_expected.to contain_file('/etc/redis/redis.conf').with(
          owner: 'redis',
          group: 'redis',
          mode: '0640',
          notify: 'Service[redis-server]',
        )
      end

      it do
        is_expected.to contain_service('redis-server').with(
          ensure: 'running',
          provider: 'systemd',
        )
      end
    end
  end
end
