require 'spec_helper'

describe 'weblate::repositories' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }
      it do
        is_expected.to contain_file('/var/lib/weblate/repositories').with(
          ensure: 'directory',
          owner: 2000000,
          group: 2000000,
          mode: '0755',
        )
      end

      it do
        is_expected.to contain_vcsrepo('/var/lib/weblate/data/vcs/tails/index').with(
          ensure: 'present',
          provider: 'git',
          remote: 'origin',
          branch: 'master',
        )
      end

      it do
        is_expected.to contain_exec('Ensure Weblate repository UID/GID for containerized Weblate').with(
          command: '/bin/chown -R 2001000:2000000 /var/lib/weblate/data/vcs',
          onlyif: '/usr/bin/find /var/lib/weblate/data/vcs -not \\( -uid 2001000 -a -gid 2000000 \\) | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_exec('Ensure Weblate repository dir is setgid').with(
          command: '/usr/bin/find /var/lib/weblate/data/vcs -type d -exec chmod 2755 \'{}\' \\;',
          onlyif: '/usr/bin/find /var/lib/weblate/data/vcs -type d -not -perm 2755 | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_exec('Ensure Weblate repository files are readable by the host\'s `weblate` group').with(
          command: '/usr/bin/find /var/lib/weblate/data/vcs -not -perm -g=r -exec chmod g+r \'{}\' \\;',
          onlyif: '/usr/bin/find /var/lib/weblate/data/vcs -not -perm -g=r | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_vcsrepo('/var/lib/weblate/repositories/integration').with(
          ensure: 'present',
          provider: 'git',
          remote: 'origin',
          branch: 'master',
        )
      end

      it do
        is_expected.to contain_exec('Ensure Integration repository UID/GID').with(
          command: '/bin/chown -R 2001000:2000000 /var/lib/weblate/repositories/integration',
          onlyif: '/usr/bin/find /var/lib/weblate/repositories/integration -not \\( -uid 2001000 -a -gid 2000000 \\) | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_vcsrepo('/var/lib/weblate/repositories/staging').with(
          ensure: 'present',
          provider: 'git',
          remote: 'origin',
          source: 'https://gitlab.tails.boum.org/tails/tails.git',
          branch: 'master',
        )
      end

      it do
        is_expected.to contain_exec('Ensure staging repository UID/GID for containerized Weblate').with(
          command: '/bin/chown -R 2001000:2000000 /var/lib/weblate/repositories/staging',
          onlyif: '/usr/bin/find /var/lib/weblate/repositories/staging -not \\( -uid 2001000 -a -gid 2000000 \\) | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_exec('Ensure staging repository is writable by the host\'s `weblate` group').with(
          command: '/usr/bin/find /var/lib/weblate/repositories/staging -exec chmod g+w \'{}\' \\;',
          onlyif: '/usr/bin/find /var/lib/weblate/repositories/staging -not -perm -g=w | /bin/grep .',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/data/vcs/tails/index/.git/config').with(
          owner: 2001000,
          group: 2000000,
          mode: '0664',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/repositories/integration/.git/config').with(
          owner: 2001000,
          group: 2000000,
          mode: '0664',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/repositories/staging/.git/config').with(
          owner: 2001000,
          group: 2000000,
          mode: '0664',
        )
      end
    end
  end
end
