require 'spec_helper'

describe 'weblate::users_and_groups' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_group('weblate').with(
          ensure: 'present',
          system: true,
          gid: 2000000,
        )
      end

      it do
        is_expected.to contain_user('weblate').with(
          ensure: 'present',
          system: true,
          home: '/var/lib/weblate',
          uid: 2000000,
          gid: 2000000,
          managehome: true,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate').with(
          ensure: 'directory',
          owner: 'weblate',
          group: 'weblate',
          mode: '0750',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/.ssh').with(
          ensure: 'directory',
          owner: 2001000,
          group: 2000000,
          mode: '0750',
        )
      end

      it do
        is_expected.to contain_exec('Ensure SSH directory\'s  UID/GID for containerized Weblate').with(
          command: '/bin/chown -R 2001000:2000000 /var/lib/weblate/.ssh',
          onlyif: '/usr/bin/find /var/lib/weblate/.ssh -not \\( -uid 2001000 -a -gid 2000000 \\) | /bin/grep .',
        )
      end
    end
  end
end
