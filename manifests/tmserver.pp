# @summary
#   A machine translation service for Weblate.
#
# @example
#   include weblate::tmserver
#
# @param bind_address
#   The address tmserver will bind to.
class weblate::tmserver (
  String $bind_address = '127.0.0.1',
) inherits weblate::params {
  # Package

  ensure_packages(['translate-toolkit', 'python3-cheroot'])

  # Database

  file { $tmserver_data_dir:
    ensure => directory,
    owner  => $system_uid,
    group  => 'tmserver',
    mode   => '0775',
  }

  file { "${tmserver_data_dir}/db":
    ensure => file,
    owner  => $system_uid,
    group  => 'tmserver',
    mode   => '0664',
  }

  # User and group

  user { 'tmserver':
    ensure  => present,
    system  => true,
    home    => $tmserver_data_dir,
    gid     => 'tmserver',
    require => Group['tmserver'],
  }

  group { 'tmserver':
    ensure => present,
    system => true,
  }

  # Systemd service

  file { '/etc/systemd/system/tmserver.service':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    require => [
      Package['translate-toolkit'],
      File["${tmserver_data_dir}/db"],
    ],
    content => "[Unit]
Description=TM Server Instance

[Service]
User=tmserver
Group=tmserver
ExecStart=/usr/bin/tmserver -d ${tmserver_data_dir}/db -b ${bind_address} -p 8888

[Install]
WantedBy=multi-user.target
",
  }

  service { 'tmserver':
    ensure    => running,
    provider  => systemd,
    require   => [
      File['/etc/systemd/system/tmserver.service']
    ],
    subscribe => File['/etc/systemd/system/tmserver.service'],
    enable    => true,
  }

  # Backups

  ensure_packages(['sqlite3'])

  file { '/etc/backup.d/20.sh':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "/usr/bin/sqlite3 ${tmserver_data_dir}/db \".backup '/var/backups/tmserver.db'\"",
    require => Package['sqlite3'],
  }

  # Periodically update the server

  file { "${weblate_scripts_dir}/update_tm.sh":
    content => epp('weblate/update_tm.sh.epp', {
        weblate_slave_languages => $weblate_slave_languages,
        weblate_data_dir        => $weblate_data_dir,
        tmserver_data_dir       => $tmserver_data_dir,
    }),
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => Package['translate-toolkit'],
  }

  cron { 'weblate update tmserver':
    command  => "${weblate_scripts_dir}/update_tm.sh",
    user     => $system_user,
    monthday => 1,
    hour     => 5,
    minute   => 23,
    require  => File["${weblate_scripts_dir}/update_tm.sh"],
  }
}
