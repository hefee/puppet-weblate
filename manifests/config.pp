# @summary
#   Put Weblate configuration in place.
#
# @example
#   include weblate::config
class weblate::config inherits weblate::params {
  include weblate::webserver

  ensure_packages(['memcached'])

  service { 'memcached':
    ensure  => running,
    require => Package['memcached'],
  }

  file { $weblate_config_dir:
    ensure => directory,
    owner  => $system_uid,
    group  => $system_gid,
  }

  file { $weblate_data_dir:
    ensure => directory,
    owner  => $ns_weblate_uid,
    group  => $ns_root_gid,
    mode   => '2755',
  }
}
