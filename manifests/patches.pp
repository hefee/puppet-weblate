# @summary
#   Patches for parts of the system so that it works as we need.
#
# @example
#   include weblate::patches
class weblate::patches {
  ensure_packages(['ikiwiki'])

  # We patch Ikiwiki's po plugin so it:
  #
  #  - is more informative when it fails to build a page, and
  #  - continues building after it finds a problem.
  #
  # TODO: will need review when there's a newer version of Ikiwiki available

  file { '/usr/share/perl5/IkiWiki/Plugin/po.pm':
    source  => 'puppet:///modules/weblate/patches/po.pm',
    mode    => '0755',
    owner   => root,
    group   => root,
    require => Package['ikiwiki'],
  }
}
