# @summary
#   Manage a Redis server as a message broker for use of Celery in Weblate.
#
# @example
#   class { 'weblate::redis':
#     redis_password => 'secret4',
#   }
#
# @param redis_password
#   The Redis password.
#
# @param bind_address
#   The address Redis will bind to.
#
class weblate::redis (
  String $redis_password,
  String $bind_address = '127.0.0.1',
) {
  package { 'redis-server':
    ensure => installed,
  }

  # This was created from the default config file distributed with Debian 9,
  # but modified so Redis is configured to:
  #
  #   - bind to 127.0.0.1 on port 6379 (this is default, but worth mentioning).
  #   - write an append only log every second.
  #   - require an authentication password (weblate::redis::password).

  file { '/etc/redis/redis.conf':
    content => epp('weblate/redis.conf.epp', {
        bind_address   => $bind_address,
        redis_password => $redis_password,
    }),
    owner   => redis,
    group   => redis,
    mode    => '0640',  # contains Redis password
    require => Package['redis-server'],
    notify  => Service['redis-server'],
  }

  service { 'redis-server':
    ensure   => running,
    provider => systemd,
    require  => File['/etc/redis/redis.conf'],
  }
}
