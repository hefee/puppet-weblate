# @summary
#   Manage the Weblate service.
#
# @example
#   class { 'weblate':
#     postgres_password      => 'secret1',
#     weblate_admin_password => 'secret2',
#     weblate_secret_key     => 'secret3',
#     redis_password         => 'secret4',
#   }
#
# @param postgres_password
#   The PostgreSQL password.
#
# @param weblate_admin_password
#   The Weblate administrative password.
#
# @param weblate_secret_key
#   The Weblate secret key.
#
# @param redis_password
#   The Redis service password.
#
# @param podman_extra_volumes
#   Extra volumes to be added to the container (used for testing purposes).
class weblate (
  String $postgres_password,
  String $weblate_admin_password,
  String $weblate_secret_key,
  String $redis_password,
  Array[String] $podman_extra_volumes = [],  # Used for testing purposes.
) inherits weblate::params {
  ### Sanity checks

  if $::lsbdistcodename != 'bullseye' {
    fail('The weblate class only supports Debian Bullseye.')
  }

  ### Weblate configuration

  include weblate::config
  include weblate::webserver

  class { 'weblate::database':
    postgres_password => $postgres_password,
  }

  class { 'weblate::redis':
    redis_password => $redis_password,
  }

  include weblate::repositories

  include weblate::logs
  include weblate::tmserver

  class { 'weblate::podman':
    redis_password         => $redis_password,
    postgres_password      => $postgres_password,
    weblate_admin_password => $weblate_admin_password,
    weblate_secret_key     => $weblate_secret_key,
    extra_volumes          => $podman_extra_volumes,
    require                => [
      Class['weblate::repositories'],
    ],
  }

  ### Integration with Tails website

  include weblate::scripts
}
