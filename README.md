# weblate

This repository contains Weblate-related code and was initially imported from
the [Tails Puppet module](https://gitlab.tails.boum.org/tails/puppet-tails.git).
