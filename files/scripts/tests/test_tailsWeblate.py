import unittest
import tailsWeblate


class TestTailsWeblate(unittest.TestCase):

    def test_addComponent(self):
        """Add a component and ensure it has all attributes needed by our code"""

        page = 'home'

        # We need to make sure a component doesn't exist before we attempt to
        # create it. As the test project was setup using Weblate's
        # `import_project` command, components in the test db end up having
        # different attribute values (name, slug, filemask) from the ones we
        # use in production. Because of that, the first run of this test will
        # not find the expected component to delete, and we fail gracefully in
        # that case. Subsequent runs of this test using the same db should
        # delete the expected component before recreating it.
        try:
            component = tailsWeblate.models.Component.objects.get(name=f'wiki/src/{page}.*.po')
            component.delete()
        except tailsWeblate.models.component.Component.DoesNotExist:
            pass

        component = tailsWeblate.addComponent(f'wiki/src/{page}')
        self.assertEqual(component.name, f'wiki/src/{page}.*.po')
        self.assertEqual(component.slug, f'wikisrc{page}-po')
        self.assertEqual(component.repo, f'weblate://tails/index')
        self.assertEqual(component.filemask, f'wiki/src/{page}.*.po')
        self.assertEqual(component.full_path, '/app/data/vcs/tails/index')
        self.assertTrue(hasattr(component, 'translation_set'))

    def test_no_slug_collision_for_version_pages(self):
        """Test that there are no slug collisions for version pages."""

        # Because Django's slugify function deletes dots, the slugs for some
        # version pages collide (eg. '4.2.2' and '4.22' both become '422' after
        # slugifying), breaking the creation of the new component when there's
        # a collision.
        #
        # Here, we just want to make sure that cases like the one we've been
        # seeing are covered.

        slug1 = tailsWeblate.slugify('wiki/src/news/version_4.22.*.po')
        slug2 = tailsWeblate.slugify('wiki/src/news/version_4.2.2.*.po')
        self.assertNotEqual(slug1, slug2, 'Same slug for different pages')
